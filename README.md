# FabLight - Prodoble
Python Process Actor for FabAccess
## Paramters
### MQTT Configuration 
* `--host` MQTT Server Address
* `--port` MQTT Server Port
* `--user` MQTT User (optional)
* `--password` MQTT Password (optional)

### FabLock Configuration
* `--fablight` FabLight ID